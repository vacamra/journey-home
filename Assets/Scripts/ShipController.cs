﻿using UnityEngine;
using System.Collections;

    public class ShipController : MonoBehaviour
    {
        Vector3 startPosition;
        Vector3 endPosition;
        ParticleSystem mEngine;
        AudioSource mEngineSound;
        ShipResources mResources;

        enum Fade { In, Out }
        public bool PlayerInSpace = true;

        public bool mMoving = false;

        // Use this for initialization
        void Start()
        {
            startPosition = endPosition = transform.position;
            mEngine = GetComponentInChildren<ParticleSystem>();
            mEngineSound = GetComponentInChildren<AudioSource>();
            mResources = GetComponent<ShipResources>();
            mMoving = false;
            mEngineSound.volume = 0;
            mEngine.enableEmission = false;
        }

        // Update is called once per frame
        void Update()
    {
        if (Input.GetMouseButtonDown(0) && !ClickedOnButton() && !mMoving && PlayerInSpace)
            {
                endPosition = Camera.main.ScreenToWorldPoint(Input.mousePosition);
                endPosition.z = 0;

                startPosition = transform.position;
            
            if (mResources.Move(Vector3.Distance(startPosition, endPosition)))
                    StartCoroutine(Move());
            }
        }

        bool ClickedOnButton() {
            var ct = UnityEngine.EventSystems.EventSystem.current;

            if (!ct.IsPointerOverGameObject()) return false;
            if (!ct.currentSelectedGameObject) return false;
            if (ct.currentSelectedGameObject.GetComponent<UnityEngine.UI.Button>() == null) return false;
                
            return true;
        }

        IEnumerator Move()
        {
            mMoving = true;

            mEngine.Play();
            StartCoroutine(AudioFade(Fade.In, 0.25f, mEngineSound));

            mEngine.enableEmission = true;

            float t = 0;
            var moveDirection = endPosition - startPosition;
            // make z part of the vector 0 as we dont need it
            moveDirection.z = 0;
            // normalize the vector so its in units of 1
            moveDirection.Normalize();

            while (t < 1)
            {
                t += Time.deltaTime; // multiply it by speed if you like
                transform.position = Vector3.Lerp(startPosition, endPosition, t);

                // if we have moved and need to rotate
                if (moveDirection != Vector3.zero)
                {
                    // calculates the angle we should turn towards, - 90 makes the sprite rotate
                    float targetAngle = Mathf.Atan2(moveDirection.y, moveDirection.x) * Mathf.Rad2Deg - 90;
                    // actually rotates the sprite using Slerp (from its previous rotation, to the new one at the designated speed.
                    transform.rotation = Quaternion.Slerp(transform.rotation, Quaternion.Euler(0, 0, targetAngle), 20 * Time.deltaTime);
                }
                
                yield return new WaitForEndOfFrame();
            }
            transform.position = endPosition;

            mEngine.enableEmission = false;

            mEngine.Stop();
            StartCoroutine(AudioFade(Fade.Out, 0.25f, mEngineSound));
            mMoving = false;
        }

        IEnumerator AudioFade(Fade fadeType, float timer, AudioSource audio)
        {
            if (fadeType == Fade.In)
            {
                audio.volume = 0;
            }

            float start = fadeType == Fade.In ? 0.0f : 1.0f;
            float end = fadeType == Fade.In ? 1.0f : 0.0f;
            var t = 0.0f;
            var step = 1.0f / timer;

            while (t <= 1.0f)
            {
                t += step * Time.deltaTime;
                audio.volume = Mathf.Lerp(start, end, t);
                yield return new WaitForEndOfFrame();
            }

            if (fadeType == Fade.Out)
            {
                audio.volume = 0;
            }
        }
    
}
