﻿using UnityEngine;
using System.Collections;

public abstract class Mission : ScriptableObject {
    public int ID;
    public string ShortDescription;
    [TextArea(3, 10)]
    public string LongDescription;
    public bool Completed;
    public Mission[] Prerequisities;
    public MonoBehaviour[] OnAccept;
    public MonoBehaviour[] OnCompletion;

    public abstract bool CheckForCompletion();
    
    public virtual string MissionGoalDescription()
    {
        return ShortDescription;
    }

    public void MissionCompleted()
    {
        Completed = true;
        foreach (var item in OnCompletion)
        {
            item.enabled = true;
        }
    }    
}
