﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class MissionUI : MonoBehaviour {
    public GameObject MissionPanel;
    public MissionLog MissionLog;
    public RectTransform NewMissionTag;

    [Header("Mission Button")]
    public Button MissionButton;
    public Text MissionButtonText;
    public Color ColorMain = Color.blue;
    public Color ColorSecondary = Color.cyan;
    public bool NewQuest = false;


    [Header("Mission Details")]
    public Text MissionTitle;
    public Text MissionDescription;
    public Color InProgress = Color.yellow;
    public Color Finished = Color.green;


    private bool missionLogOpen = false;

	// Use this for initialization
	void Start () {
        MissionTitle.color = Finished;
        MissionPanel.GetComponent<CanvasGroup>().alpha = 0;
        MissionPanel.SetActive(false);

    }

    // Update is called once per frame
    void Update () {
        if (NewQuest) {
            StartCoroutine(AnimateNewMission(true));
        }
    }

    public void DisplayMissionPanel()
    {
        /*RectTransform rt = MissionPanel.GetComponent<RectTransform>();
        
        Vector3 position = rt.position;
        position.x += rt.rect.width;
        rt.position = position;*/
        if (missionLogOpen)
            HideMissionPanel();
        else
        {
            StartCoroutine(AnimateMission(true));
            missionLogOpen = true;
        }
    }

    public void HideMissionPanel()
    {
        /*RectTransform rt = MissionPanel.GetComponent<RectTransform>();

        Vector3 position = rt.position;
        position.x -= rt.rect.width;
        rt.position = position;*/
        StartCoroutine(AnimateMission(false));
        missionLogOpen = false;
        NewQuest = false;

        StartCoroutine(AnimateNewMission(NewQuest));
    }



    public void SetNewMission(Mission mission)
    {
        NewQuest = true;

        MissionTitle.text = mission.ShortDescription ?? "Null";
        MissionTitle.color = mission.Completed ? Finished : InProgress;

        MissionDescription.text = mission.LongDescription ?? "Null";
    }

    IEnumerator AnimateMission(bool open)
    {
        MissionPanel.SetActive(true);
        CanvasGroup rt = MissionPanel.GetComponent<CanvasGroup>();
        float alpha = rt.alpha;

        float t = 0;

        while (t < 1)
        {
            t += Time.deltaTime * 5f;
            alpha = open ? t : 1 - t;
            rt.alpha = alpha;

            yield return new WaitForEndOfFrame();
        }
        MissionPanel.SetActive(open);
    }

    IEnumerator AnimateNewMission(bool show) {
        RectTransform rt = NewMissionTag;
        Vector2 transform = rt.sizeDelta;
        if ((show && transform.x == 0 )|| (!show && transform.x == 180))
        {
            float t = 0;

            while (t < 180)
            {
                t += Time.deltaTime * 900f;
                transform.x = Mathf.Clamp(show ? t : 180 - t,0,180);

                rt.sizeDelta = transform;

                yield return new WaitForEndOfFrame();
            }
        }
    }
}
