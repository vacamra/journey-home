﻿using UnityEngine;
using System.Collections;


public class DontDestroy : MonoBehaviour
{
    static GameObject GameLogic;
    static GameObject AudioInstance;

    void Awake() {
        if (gameObject.GetComponent<AudioListener>() != null)
        {
            if (AudioInstance != null)
            {
                Destroy(gameObject);
            }
            else
            {
                AudioInstance = gameObject;
                DontDestroyOnLoad(this);
            }
        }
        if (gameObject.GetComponent<GameLogicInit>() != null)
        {
            if (GameLogic != null)
            {
                Destroy(gameObject);
            }
            else
            {
                GameLogic = gameObject;
                DontDestroyOnLoad(this);
            }
        }
    }

	// Use this for initialization	
	void Start () {

	}

	// Update is called once per frame
	void Update () {
				
	}
}
		