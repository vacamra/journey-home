﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class OutroControls : MonoBehaviour {

    public RectTransform ScrollableText;
    public RectTransform Canvas;
    public Text GameDev;
    bool triggerExit = false;
    float positionStart = 0f;
	// Use this for initialization	
	void Start () {
        var color = GameDev.color;
        color.a = 0;
        GameDev.color = color;
        positionStart = ScrollableText.transform.position.y;
    }

	// Update is called once per frame
	void Update () {
        var pos = Camera.main.transform.position;
        pos.y += 0.017f;
        pos.x += 0.1f;
        Camera.main.transform.position = pos;

        pos = ScrollableText.transform.position;
        pos.y += 2f;
        ScrollableText.transform.position = pos;


        Vector3[] v = new Vector3[4];
        ScrollableText.GetWorldCorners(v);

        float maxY = Mathf.Max(v[0].y, v[1].y, v[2].y, v[3].y);
        float minY = Mathf.Min(v[0].y, v[1].y, v[2].y, v[3].y);
        if ((maxY < 0 || minY > Screen.height)&&!triggerExit && pos.y > 100)
        {
            StartCoroutine(Exit());
            triggerExit = true;
        }
        else
        {
            // Do something that re-enable UI elements
        }

    }

    IEnumerator Exit()
    {
        var color = GameDev.color;
        color.a = 0;

        float t = 0;
        while (t < 1)
        {
            t += Time.deltaTime * 0.7f;

            GameDev.color = color;
            color.a = Mathf.Clamp01(t);
            yield return new WaitForEndOfFrame();
        }

        yield return new WaitForSeconds(10);
        Application.LoadLevel("IntroScene");
    } 
}
		