﻿using UnityEngine;
using System.Collections;

public class RestartGame : MonoBehaviour {

	// Use this for initialization
	void Start () {
        Application.UnloadLevel("SpaceScene");
        Application.LoadLevel("SpaceScene");
	}
}
