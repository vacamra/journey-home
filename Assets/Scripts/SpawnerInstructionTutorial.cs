﻿using UnityEngine;
using System.Collections;

namespace JourneyHome
{
    public class SpawnerInstructionTutorial : MonoBehaviour
    {
        Spawner spawner;
        float lastSpawnTime;
        // Use this for initialization
        void Start()
        {
            spawner = this.gameObject.GetComponent<Spawner>();
        }

        // Update is called once per frame
        void Update()
        {
            if (Time.time - lastSpawnTime >= 5)
            {
                spawner.SpawnAt(ResourceType.Fuel, Random.Range(0, 100) * Random.insideUnitCircle);
                lastSpawnTime = Time.time;
            }            
        }
    }
}
