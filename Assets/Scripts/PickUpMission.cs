﻿using UnityEngine;
using System.Collections.Generic;
using System;

public class PickUpMission : Mission
{
    public List<GameObject> itemsToBePicked = null;

    // Update is called once per frame
    public override bool CheckForCompletion()
    {
        for (int i = itemsToBePicked.Count - 1; i >= 0; i--)
        {
            if (itemsToBePicked[i] == null)
            {
                itemsToBePicked.RemoveAt(i);
            }
        }
        return itemsToBePicked.Count == 0;
    }

    public void Init(List<GameObject> toBePickedUp)
    {
        itemsToBePicked = toBePickedUp;
    }
}
