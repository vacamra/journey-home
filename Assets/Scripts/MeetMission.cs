﻿using UnityEngine;
using System.Collections;
using System;

public class MeetMission : Mission {
    public Collider2D Collider1;
    public Collider2D Collider2;
	

    public override bool CheckForCompletion()
    {
        return Collider1.bounds.Intersects(Collider2.bounds);
    }

    public void Init(Collider2D col1, Collider2D col2)
    {
        this.Collider1 = col1;
        this.Collider2 = col2;
    }
}
