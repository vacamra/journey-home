﻿using UnityEngine;
using UnityEngine.UI;

public class PlanetEntrance : MonoBehaviour {
    private Planet mPlanet;
    private GameObject mPlayer;

    public Text TimerText;

    private float mFadeTimer = 1.0f;

    private float mTriggerTime = 0;
    private float mTriggerStartTime = 0;

    bool timerDisplayed = false;
    bool canEnter = true;

    private string timerString {
        get
        {
            return string.Format("Entering in {0:0.00} s", mTriggerTime);
        }
    }

    // Use this for initialization
    void Start ()
    {
        mPlayer = GameObject.Find("Player");
        mPlanet = transform.parent.GetComponent<Planet>();

        TimerText.color = Color.clear;
    }
	
	// Update is called once per frame
	void Update () {
        FadeText();
	}

    void FadeText() {
        if (timerDisplayed)
        {
            mTriggerTime = mPlanet.EnterTime - (Time.time - mTriggerStartTime);
            if (mTriggerTime < 0) {
                timerDisplayed = false;
                mTriggerTime = 0;
                TimerText.text = timerString;
                canEnter = false;
                mPlanet.PlayerEnter();
                return;
            }

            TimerText.text = timerString;
            TimerText.color = Color.Lerp(TimerText.color, Color.white, mFadeTimer * Time.deltaTime);
        }
        else
        {
            TimerText.color = Color.Lerp(TimerText.color, Color.clear, mFadeTimer * Time.deltaTime);
        }
    }

    public void SetPlanet(Planet planet) {
        mPlanet = planet;
    }

    void OnTriggerEnter2D(Collider2D other)
    {
        if (canEnter == false)
        {
            canEnter = true;
            return;
        }
        if (other.gameObject == mPlayer)
        {
            timerDisplayed = true;
            mTriggerStartTime = Time.time;
            Debug.Log("Entering");
        }
    }
    void OnTriggerExit2D(Collider2D other)
    {
        if (other.gameObject == mPlayer)
        {
            Debug.Log("Left Entering");
            timerDisplayed = false;
        }
    }
}
