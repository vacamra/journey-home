﻿using UnityEngine;
using System.Collections;

public class SpawnObjectsInCircularArea {
    public Spawner spawner;
    public GameObject center;
    public float minRadius;
    public float radius;
    public int spawnedObjectCount;
    public ResourceType spawnedObjectType;
    public PickUpMission mission;
    public Minimap minimap;
    
	public void Spawn () {
        Vector2 centerPosition = new Vector2(center.transform.position.x, center.transform.position.y);
        for (int i = 0; i < spawnedObjectCount; i++)
        {
            Vector2 position = Random.insideUnitCircle * radius;
            if (position.magnitude < minRadius) position = position.normalized * minRadius;
            GameObject go = spawner.SpawnAt(spawnedObjectType, centerPosition + position);
            if (mission != null)
            {
                mission.itemsToBePicked.Add(go);
                minimap.StartTracking(go, Minimap.MinimapIconType.Collectible);
            }            
        }
	}
	
}
