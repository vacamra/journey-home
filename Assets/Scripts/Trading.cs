﻿using UnityEngine;
using UnityEngine.UI;

//Currently the trade ratio is 1:1
public class Trading: MonoBehaviour {
    public ResourceLevels ShipResources;
    public ResourceLevels PlanetResources;

    public Slider ShipFuelSlider;
    public Slider ShipScrapSlider;
    public Slider ShipOxygenSlider;

    public Image PlanetFuelImage;
    public Image PlanetScrapImage;
    public Image PlanetOxygenImage;

    private float tradeAmount {
        get {
            return shipsFuelDifference + shipsOxygenDifference;
        }
    } 

    private float startFuel;
    private float startFuelP;
    private float shipsFuelDifference
    {
        get
        {
            return startFuel - ShipResources.Fuel;
        }
    }
    private float shipsFuelTradeCap
    {
        get
        {
            return Mathf.Clamp(startFuel + startFuelP + tradeAmount, 0, ShipResources.FuelCap);
        }
    }

    private float startScrap;
    private float startScrapP;
    private float shipsScrapDifference
    {
        get
        {
            return startScrap - ShipResources.Scrap;
        }
    }
    private float shipsScrapTradeCap
    {
        get
        {
            if (tradeAmount < 0) return ShipResources.Fuel;
            return Mathf.Clamp(startScrap + startScrapP, 0, ShipResources.ScrapCap);
        }
    }

    private float startOxygen;
    private float startOxygenP;
    private float shipsOxygenDifference
    {
        get
        {
            return startOxygen - ShipResources.Oxygen;
        }
    }
    private float shipsOxygenTradeCap
    {
        get
        {
            return Mathf.Clamp(startOxygen + startOxygenP + tradeAmount, 0, ShipResources.OxygenCap);
        }
    }

	// Use this for initialization
	void Start () {
        foreach (var item in DisabledGameObjects.disabledGameObjects)
        {
            if (item.name == "Player")
            {
                ShipResources = item.GetComponent<ResourceLevels>();
                break;
            }
        }
        PlanetResources = Planet.activePlanet.GetComponent<ResourceLevels>();

        // Fuel initiali8zation
        startFuel = ShipResources.Fuel;
        startFuelP = PlanetResources.Fuel;
        ShipFuelSlider.maxValue = ShipResources.FuelCap;
        ShipFuelSlider.value = ShipResources.Fuel;
        PlanetFuelImage.fillAmount = PlanetResources.FuelLevel;


        startScrap = ShipResources.Scrap;
        startScrapP = PlanetResources.Scrap;
        ShipScrapSlider.maxValue = ShipResources.ScrapCap;
        ShipScrapSlider.value = ShipResources.Scrap;
        PlanetScrapImage.fillAmount = PlanetResources.ScrapLevel;


        startOxygen = ShipResources.Oxygen;
        startOxygenP = PlanetResources.Oxygen;
        ShipOxygenSlider.maxValue = ShipResources.OxygenCap;
        ShipOxygenSlider.value = ShipResources.Oxygen;
        PlanetOxygenImage.fillAmount = PlanetResources.OxygenLevel;
    }
	
	// Update is called once per frame
	void Update () {
        PlanetFuelImage.fillAmount = PlanetResources.FuelLevel;
        PlanetScrapImage.fillAmount = PlanetResources.ScrapLevel;
        PlanetOxygenImage.fillAmount = PlanetResources.OxygenLevel;
    }

    public void OnFuelChanged()
    {
        Debug.Log("TradeDif: " + tradeAmount);
        float deltaFuel = ShipFuelSlider.value - ShipResources.Fuel;

        if (deltaFuel > 0) // Fuel bought
        {
            deltaFuel = Mathf.Min(PlanetResources.Fuel, deltaFuel, ShipResources.Scrap, PlanetResources.ScrapCap - PlanetResources.Scrap);
        }
        else // Fuel sold
        {
            deltaFuel = - Mathf.Min(PlanetResources.FuelCap - PlanetResources.Fuel, -deltaFuel, ShipResources.ScrapCap - ShipResources.Scrap, PlanetResources.Scrap);
        }

        ShipResources.Fuel += deltaFuel;
        PlanetResources.Fuel -= deltaFuel;

        ShipResources.Scrap -= deltaFuel;
        PlanetResources.Scrap += deltaFuel;

        ShipFuelSlider.value = ShipResources.Fuel;
        ShipScrapSlider.value = ShipResources.Scrap; 
    }
    public void OnScrapChanged()
    {
        /*Debug.Log("TradeDif: " + tradeAmount);

        if (ShipScrapSlider.value > shipsScrapTradeCap)
            ShipScrapSlider.value = shipsScrapTradeCap;

        ShipResources.Scrap = ShipScrapSlider.value;
        PlanetResources.Scrap = startScrapP + shipsScrapDifference;*/
    }
    public void OnOxygenChanged()
    {
        Debug.Log("TradeDif: " + tradeAmount);
        float deltaOxygen = ShipOxygenSlider.value - ShipResources.Oxygen;

        if (deltaOxygen > 0) // Oxygen bought
        {
            deltaOxygen = Mathf.Min(PlanetResources.Oxygen, deltaOxygen, ShipResources.Scrap, PlanetResources.ScrapCap - PlanetResources.Scrap);
        }
        else // Oxygen sold
        {
            deltaOxygen = -Mathf.Min(PlanetResources.OxygenCap - PlanetResources.Oxygen, -deltaOxygen, ShipResources.ScrapCap - ShipResources.Scrap, PlanetResources.Scrap);
        }

        ShipResources.Oxygen += deltaOxygen;
        PlanetResources.Oxygen -= deltaOxygen;

        ShipResources.Scrap -= deltaOxygen;
        PlanetResources.Scrap += deltaOxygen;

        ShipOxygenSlider.value = ShipResources.Oxygen;
        ShipScrapSlider.value = ShipResources.Scrap;
    }
}
