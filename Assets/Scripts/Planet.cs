﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections.Generic;

public class Planet : MonoBehaviour {
    public static Planet activePlanet;
    public string Name = "Planet";
    public Color IconColor = Color.black;
    public Sprite PlanetBackground;

    public ResourceLevels ResourceLevels;
    public float EnterTime = 2;

    public Canvas ResourceCanvas;
    private CanvasGroup mGroup;
    public Text FuelText;
    public Text OxygenText;
    public Text ScrapText;

    private float mFadeTime = 5.0f;

    private bool mResourcesDisplayed = false;

    private GameObject mPlayer;
    private PlanetEntrance mEntrance;
    

    // Use this for initialization
    void Start () {
        mPlayer = GameObject.Find("Player");
        mEntrance = GetComponentInChildren<PlanetEntrance>();
        mEntrance.SetPlanet(this);

        mGroup = ResourceCanvas.GetComponent<CanvasGroup>();
    }
	
	// Update is called once per frame
	void Update ()
    {
        if(mResourcesDisplayed)
        {
            mGroup.alpha = Mathf.Lerp(mGroup.alpha, 1, Time.deltaTime* mFadeTime);
        }
        else
        {
            mGroup.alpha = Mathf.Lerp(mGroup.alpha, 0, Time.deltaTime* mFadeTime);
        }
    }

    public void PlayerEnter() {
        activePlanet = this;
        if (activePlanet.Name == "Earth") QuestGiver.questGiver.CanOffer[QuestGiver.PICKUP_DEBRIS] = true;
        if (activePlanet.Name == "Mars") QuestGiver.questGiver.CanOffer[QuestGiver.TRADE] = true;

        foreach (GameObject gameObject in GameObject.FindObjectsOfType<GameObject>())
        {
            if (gameObject.name == "GameLogic") { } // Dont disable this because of sound looping
            else
            if (gameObject.name == "AudioListener") { } // Dont disable this because of sound looping
            else
            if (gameObject.activeSelf && gameObject.name != "QuestGiver")
            {
                gameObject.SetActive(false);
                DisabledGameObjects.disabledGameObjects.Add(gameObject);
            }            
        }

        mPlayer.GetComponent<ShipController>().PlayerInSpace = false;
        Application.LoadLevelAdditive("PlanetDetails");

    }

    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject == mPlayer)
        {
            mResourcesDisplayed = true;

            FuelText.text = ((int)ResourceLevels.GetResource(ResourceType.Fuel)).ToString();
            OxygenText.text = ((int)ResourceLevels.GetResource(ResourceType.Oxygen)).ToString();
            ScrapText.text = ((int)ResourceLevels.GetResource(ResourceType.Scrap)).ToString();
        }
    }
    void OnTriggerExit2D(Collider2D other)
    {
        if (other.gameObject == mPlayer)
        {
            mResourcesDisplayed = false;
        }
    }
}

class DisabledGameObjects
{
    public static List<GameObject> disabledGameObjects = new List<GameObject>();    
}