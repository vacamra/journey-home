﻿using UnityEngine;
using System.Collections.Generic;

public class MissionLog : MonoBehaviour{
    private HashSet<Mission> finishedMissions = new HashSet<Mission>();
    private HashSet<Mission> acceptedMissions = new HashSet<Mission>();
    public MissionUI missionUI;

    public GameObject player;

    public static MissionLog log;

    void Start() {
        log = this;
    }

    public void Update()
    {
        List<Mission> completedMissions = new List<Mission>();
        foreach (var mission in acceptedMissions)
        {
            if (mission.CheckForCompletion())
            {
                completedMissions.Add(mission);
            }
        }
        foreach (var mission in completedMissions)
        {
            Complete(mission);
        }
    }

    public bool CanAccept(Mission mission)
    {
        foreach (var prereq in mission.Prerequisities)
        {
            if (!finishedMissions.Contains(prereq))
            {
                return false;
            }
        }
        return true;
    }

    public void Accept(Mission mission)
    {
        Debug.Log("Accepting mission " + mission.ShortDescription);        
        if (!acceptedMissions.Contains(mission))
        {
            acceptedMissions.Add(mission);
            missionUI.SetNewMission(mission);
        }       
    }

    public void Complete(Mission mission)
    {
        Debug.Log("Completing mission");
        acceptedMissions.Remove(mission);
        finishedMissions.Add(mission);
        mission.MissionCompleted();
    }
    
    public Mission GetActive()
    {
        foreach (Mission item in acceptedMissions)
        {
            return item; // there should be only one
        }
        return null;
    }
}
