﻿using UnityEngine;
using System.Collections;
using System;

public enum ResourceType { Fuel, Scrap, Oxygen}

public class ResourceLevels : MonoBehaviour
{
    public float Fuel;
    public float Scrap;
    public float Oxygen;

    public float FuelCap = 100f;
    public float ScrapCap = 100f;
    public float OxygenCap = 100f;

    public float GetResource(ResourceType type)
    {
        switch (type)
        {
            case ResourceType.Fuel: return Fuel;
            case ResourceType.Scrap: return Scrap;
            case ResourceType.Oxygen: return Oxygen;
        }
        throw new ArgumentException();
    }

    public float FuelLevel
    {
        get
        {
            return Mathf.Clamp01(Fuel / FuelCap);
        }
    }
    public float ScrapLevel
    {
        get
        {
            return Mathf.Clamp01(Scrap / ScrapCap);
        }
    }
    public float OxygenLevel
    {
        get
        {
            return Mathf.Clamp01(Oxygen / OxygenCap);
        }
    }
}
