﻿using UnityEngine;
using System.Collections;

public class QuestGiver : MonoBehaviour {
    public MeetMission GoToEarthMission;
    public PickUpMission PickUpDebrisMission;
    public MeetMission GoToMarsMission;
    public ResourceLevelMission TradeMission;
    public MeetMission ExploreAnomalyMission;

    public Collider2D Player;
    public GameObject EarthObject;
    public Collider2D Earth;
    public GameObject MarsObject;
    public Collider2D Mars;
    public GameObject AnomalyObject;
    public Collider2D Anomaly;

    public MissionLog Log;
    public Spawner spawner;
    public Minimap minimap;

    private bool[] missionCompleted = new bool[5];
    public bool[] CanOffer = new bool[5];
    public const int GO_TO_EARTH = 0;
    public const int PICKUP_DEBRIS = 1;
    public const int GO_TO_MARS = 2;
    public const int TRADE = 3;
    public const int EXPLORE_ANOMALY = 4;

    public static QuestGiver questGiver;


    void Start () {
        questGiver = this;
        GoToEarthMission.Collider1 = Player;
        GoToEarthMission.Collider2 = Earth;

        GoToMarsMission.Collider1 = Player;
        GoToMarsMission.Collider2 = Mars;
        
        ExploreAnomalyMission.Collider1 = Player;
        ExploreAnomalyMission.Collider2 = Anomaly;

        TradeMission.resources = Player.GetComponent<ResourceLevels>();

        for (int i = 0; i < 5; i++)
        {
            missionCompleted[i] = false;
            CanOffer[i] = false;
        }
        CanOffer[0] = true;

        GoToEarthMission.Completed = false;
        PickUpDebrisMission.Completed = false;
        GoToMarsMission.Completed = false;
        TradeMission.Completed = false;
        ExploreAnomalyMission.Completed = false;

        Log.Accept(GoToEarthMission);
        minimap.StartTracking(EarthObject, Minimap.MinimapIconType.Quest);
	}
	
	// Update is called once per frame
	public void Update () {
        Log.Update();
        if (missionCompleted[0] != GoToEarthMission.Completed  && CanOffer[1])
        {
            missionCompleted[0] = true;
            minimap.StopTracking(EarthObject);

            new SpawnObjectsInCircularArea
            {
                center = Earth.gameObject,
                mission = PickUpDebrisMission,
                minRadius = 10,
                radius = 50,
                spawnedObjectCount = 10,
                spawnedObjectType = ResourceType.Scrap,
                spawner = spawner,
                minimap = minimap
            }.Spawn();
            Log.Accept(PickUpDebrisMission);
            CanOffer[GO_TO_MARS] = true;
        }
        else if (missionCompleted[1] != PickUpDebrisMission.Completed && CanOffer[2])
        {
            missionCompleted[1] = true;
            minimap.StartTracking(MarsObject, Minimap.MinimapIconType.Quest);

            Log.Accept(GoToMarsMission);
        }
        else if (missionCompleted[2] != GoToMarsMission.Completed && CanOffer[3])
        {
            missionCompleted[2] = true;
            minimap.StopTracking(MarsObject);

            TradeMission.requiredResource = ResourceType.Scrap;
            TradeMission.requiredResourceIncrease = -100;
            TradeMission.resources = Player.GetComponent<ResourceLevels>();
            TradeMission.Init(TradeMission.resources);
            Log.Accept(TradeMission);
            CanOffer[4] = true;
        }
        else if (missionCompleted[3] != TradeMission.Completed && CanOffer[4])
        {            
            missionCompleted[3] = true;
            minimap.StartTracking(Anomaly.gameObject, Minimap.MinimapIconType.Quest);

            Log.Accept(ExploreAnomalyMission);
        }
        else if (missionCompleted[4] != ExploreAnomalyMission.Completed)
        {
            //TODO Finish animation



        }
	}
    
}


