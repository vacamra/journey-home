﻿using UnityEngine;
using System.Collections;

public class MinimapPopulater : MonoBehaviour {
    public Minimap minimap;
    public GameObject[] ShownItems;

	// Use this for initialization
	void Start () {
        foreach (var item in ShownItems)
        {
            minimap.StartTracking(item, Minimap.MinimapIconType.Planet);
        }
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
