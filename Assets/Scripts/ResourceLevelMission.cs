﻿using UnityEngine;
using System.Collections;
using JourneyHome;

public class ResourceLevelMission : Mission
{
    public float requiredResourceIncrease;
    public ResourceType requiredResource;
    public ResourceLevels resources;

    private float resourceLevelOnAccept;
    
	public override bool CheckForCompletion()
    {
        Debug.Log(string.Format("Check For Completion (ResourceLevelMission): Required {0}, Current {1}", 
            requiredResourceIncrease, resources.GetResource(requiredResource) - resourceLevelOnAccept));
        if (requiredResourceIncrease > 0)
        {
            return resources.GetResource(requiredResource) >= requiredResourceIncrease + resourceLevelOnAccept;
        }
        else
        {
            return resources.GetResource(requiredResource) <= resourceLevelOnAccept + requiredResourceIncrease;
        }        
    }

    public override string MissionGoalDescription()
    {
        return ShortDescription + ": " + (int)(resources.GetResource(requiredResource) - resourceLevelOnAccept) + "/" + (int)requiredResourceIncrease;
    }

    public void Init(ResourceLevels resources)
    {
        this.resources = resources;
        Init();
    }

    public void Init()
    {
        resourceLevelOnAccept = resources.GetResource(requiredResource);
    }
}
