﻿using UnityEngine;
using System.Collections;

public class IntroController : MonoBehaviour {
    Vector3 cameraPos;
    GameObject camera;

	// Use this for initialization
	void Start () {
        Debug.Log("Started");
        camera = GameObject.Find("Main Camera");
	}
	
	// Update is called once per frame
	void Update () {
        cameraPos = camera.transform.position;
        cameraPos.x += Time.deltaTime*20f;
        camera.transform.position = cameraPos;

        if (Input.GetKey(KeyCode.Space) || Input.GetMouseButtonDown(0)) {
            Invoke("Change",0);
        }
    }
    
    void Change() {
        Debug.Log("Changing");

        StartCoroutine(GoToTutorial());
    }

    IEnumerator GoToTutorial() {
        float fadeTime = GetComponent<Fading>().BeginFade(1);
        yield return new WaitForSeconds(fadeTime);
        Debug.Log("Loading");

        Application.LoadLevel("PeterTestScene");
    }
}
