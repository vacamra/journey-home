﻿using UnityEngine;
using System.Collections;

public class BackgroundFollowCamera : MonoBehaviour {

    private Material mMaterial;
    private Vector2 mOffset;
    // Use this for initialization
    void Start () {
        MeshRenderer mr = GetComponent<MeshRenderer>();
        mMaterial = mr.material;
    }
	
	// Update is called once per frame
	void Update () {

        mOffset = mMaterial.mainTextureOffset;


        mOffset.x = transform.position.x / transform.localScale.x;
        mOffset.y = transform.position.y / transform.localScale.y;

        mMaterial.mainTextureOffset = mOffset;
	}
}
