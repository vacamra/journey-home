﻿using UnityEngine;
using System.Collections.Generic;

public class Minimap : MonoBehaviour {
    public GameObject Player;
    public Sprite MinimapQuestIcon;
    public Sprite MinimapPlanetIcon;
    public Sprite MinimapCollectibleIcon;

    private SpriteRenderer mRenderer;
    private Color transparentRenderer;
    private Color visibleRenderer;
    
    private Dictionary<GameObject, GameObject> trackedItems = new Dictionary<GameObject, GameObject>();
    private float minimapImageRadius;
    
    public enum MinimapIconType { Quest, Planet, Collectible}

	// Use this for initialization
	void Start () {
        Vector3 minimapCircleSize = GetComponent<Renderer>().bounds.size;
        minimapImageRadius = minimapCircleSize.x / 2;

        mRenderer = GetComponent<SpriteRenderer>();
        visibleRenderer = mRenderer.color;
        transparentRenderer = mRenderer.color;
        transparentRenderer.a = 0;
	}
	
	// Update is called once per frame
	void Update () {
        bool visible = false;


            foreach (var item in trackedItems)
            {
                if (item.Key == null) continue;
                Renderer objectRenderer = item.Key.GetComponent<Renderer>();
                Renderer iconRenderer = item.Value.GetComponent<Renderer>();
                if (objectRenderer.isVisible)
                {
                    iconRenderer.enabled = false;
                }
                else
                {
                    visible = true;
                    iconRenderer.enabled = true;
                    Vector3 vector = item.Key.transform.position - transform.position;
                    Vector3 iconPosition = transform.position + minimapImageRadius * vector.normalized;
                    iconPosition.z -= 0.1f;
                    item.Value.transform.position = iconPosition;
            }

            if (!visible)
            {
                mRenderer.color = Color.Lerp(mRenderer.color, transparentRenderer, Time.deltaTime*2);
            }
            else
            {
                mRenderer.color = Color.Lerp(mRenderer.color, visibleRenderer, Time.deltaTime*2);
            }
        } 
	}

    public void StopTracking(GameObject gameObject)
    {
        trackedItems.Remove(gameObject);
    }

    public void StartTracking(GameObject gameObject, MinimapIconType type)
    {
        GameObject tracker = new GameObject("Tracker");
        SpriteRenderer renderer = tracker.AddComponent<SpriteRenderer>();

        switch (type)
        {
            case MinimapIconType.Quest:
                renderer.sprite = MinimapQuestIcon;
                break;
            case MinimapIconType.Planet:
                renderer.sprite = MinimapPlanetIcon;
                renderer.color = gameObject.GetComponent<Planet>().IconColor;
                break;
            case MinimapIconType.Collectible:
                renderer.sprite = MinimapQuestIcon;
                break;
        }

        tracker.transform.SetParent(transform);
        trackedItems[gameObject] = tracker;
    }
}
