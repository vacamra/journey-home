﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class Anomaly : MonoBehaviour {
    public GameObject Player;
    private LensFlare mExplosionFlare;

    public ParticleSystem Expansion;
    private float detectionRadius;

    private bool mPlayerNear;
    private bool mExploded;

    // Use this for initialization
    void Start () {
        mExplosionFlare = GetComponentInChildren<LensFlare>();
        detectionRadius = GetComponent<CircleCollider2D>().radius;
    }

    // Update is called once per frame
    void Update () {
        if (mPlayerNear && !mExploded) {
            float distance = Vector3.Distance(Player.transform.position, transform.position);
            Expansion.emissionRate = detectionRadius - distance;

            if (distance < detectionRadius / 10f) {
                //Triger player warp and explode the anomaly
                StartCoroutine(Explode());
                Expansion.Stop();
                mExploded = true;
            }
        }
	}

    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject == Player)
        {
            mPlayerNear = true;
            Expansion.Play();
        }
    }
    void OnTriggerExit2D(Collider2D other)
    {
        if (other.gameObject == Player)
        {
            mPlayerNear = false;
            Expansion.Stop();
        }
    }

    IEnumerator Explode() {
        float t = 0;
        mExplosionFlare.brightness = 0.1f;


        GameObject.Find("FinishCurian").SetActive(true);
        var curtian = GameObject.Find("FinishCurian").GetComponent<SpriteRenderer>();
        var color = curtian.color;

        while (t < 1)
        {
            t += Time.deltaTime; // multiply it by speed if you like
            mExplosionFlare.brightness = Mathf.Lerp(mExplosionFlare.brightness, 2, t*0.1f);
            yield return new WaitForEndOfFrame();
        }

        t = 0;

        while (t < 1)
        {
            t += Time.deltaTime; // multiply it by speed if you like
            mExplosionFlare.brightness = Mathf.Lerp(mExplosionFlare.brightness, 20, t*5);
            if (t > 0.5) {

                color.a = Mathf.Clamp01(t-0.5f);
                curtian.color = color;
            }

            yield return new WaitForEndOfFrame();
        }

        t = 0;

        while (t < 0.5f)
        {
            t += Time.deltaTime; // multiply it by speed if you like
            mExplosionFlare.brightness = Mathf.Lerp(mExplosionFlare.brightness, 0, t);
            color.a = Mathf.Clamp01(0.5f+t);
            curtian.color = color;
            yield return new WaitForEndOfFrame();
        }

        yield return new WaitForSeconds(1);
        //transform.parent.gameObject.SetActive(false); //TODO: Invoke game controller spawner to spawn new enviroment
        StartCoroutine(GoToOutro());
    }

    IEnumerator GoToOutro()
    {
        float fadeTime = GameObject.Find("EventSystem").GetComponent<Fading>().BeginFade(1);
        yield return new WaitForSeconds(fadeTime);
        Application.LoadLevel("CreditsScene");
    }
}
