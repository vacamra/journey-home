﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class ShipResources : MonoBehaviour
{

    public Slider SliderFuel;
    public Slider SliderScrap;
    public Slider SliderOxygen;

    public ResourceLevels resources;

    public float FuelConsumption = 0.15f;
    
    // Update is called once per frame
    void Update()
    {
        SliderFuel.value = Mathf.Lerp(SliderFuel.value, resources.FuelLevel, Time.deltaTime);
        SliderOxygen.value = Mathf.Lerp(SliderOxygen.value, resources.OxygenLevel, Time.deltaTime);
        SliderScrap.value = Mathf.Lerp(SliderScrap.value, resources.ScrapLevel, Time.deltaTime);
    }

    public bool Move(float distance)
    {
        float consumed = distance * FuelConsumption;
        if (resources.Fuel < 1) Application.LoadLevel(1); 
        if (consumed > resources.Fuel)
        {
            return false;  // Ship is unable to move
        }

        resources.Fuel = resources.Fuel - consumed;
        return true;
    }

    public void Add(ResourceType type, float volume)
    {
        switch (type)
        {
            case ResourceType.Fuel:
                resources.Fuel += volume;
                break;
            case ResourceType.Scrap:
                resources.Scrap += volume;
                break;
            case ResourceType.Oxygen:
                resources.Oxygen += volume;
                break;
        }
    }

    public float GetResourceLevel(ResourceType type)
    {
        switch (type)
        {
            case ResourceType.Fuel: return resources.Fuel;
            case ResourceType.Scrap: return resources.Scrap;
            case ResourceType.Oxygen: return resources.Oxygen;
            default: return 0;
        }
    }
}
