﻿using UnityEngine;
using System.Collections;


public class FadeOutPanel : MonoBehaviour {
    
	// Use this for initialization	
	void Start () {
        Invoke("fade", 2); 
	}

	// Update is called once per frame
	void Update () {
				
	}
    void fade() {
        StartCoroutine(Fadeout());
    }

    IEnumerator Fadeout() {
        float t = 0;
        while (t < 1)
        {
            t += Time.deltaTime * 0.7f;

            GetComponent<CanvasGroup>().alpha = Mathf.Clamp01(1-t);
            yield return new WaitForEndOfFrame();
        }
        Destroy(gameObject);

    }
}
		