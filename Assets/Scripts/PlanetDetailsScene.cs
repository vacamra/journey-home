﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class PlanetDetailsScene : MonoBehaviour {

    public Planet DisplayedPlanet;

    [Header("Interface components")]
    public Text PlanetTitle;
    public Text MissionTitle;
    public Text MissionDescription;
    public Button MissionButton;
    public Button TradeButton;
    public Button ExitButton;
    public Image Background;

    [Header("Resources View")]
    //public Canvas Resources;

    [Header("Details View")]
    public Canvas Details;
    public Canvas MissionUI;
    public Canvas TradeUI;

    private bool mDetailsDisplayed;
    private bool mAnimationInProgress;
    private int mSelectedDisplay = MAIN_DISPLAY;
    private const int MAIN_DISPLAY = 0;
    private const int MISSION_DISPLAY = 1;
    private const int TRADE_DISPLAY = 2;

    // Use this for initialization
    void Start () {
        if (DisplayedPlanet == null)
        {
            DisplayedPlanet = Planet.activePlanet;
        }
        PlanetTitle.text = DisplayedPlanet.Name;

        //Background.sprite = DisplayedPlanet.PlanetBackground;

        Color baseColor = new Color(DisplayedPlanet.IconColor.r, DisplayedPlanet.IconColor.g, DisplayedPlanet.IconColor.b, 0.5f);
        Color higColor = new Color(Mathf.Clamp01(DisplayedPlanet.IconColor.r - 0.1f), Mathf.Clamp01(DisplayedPlanet.IconColor.g - 0.1f), Mathf.Clamp01(DisplayedPlanet.IconColor.b-0.1f), 0.8f);

        ColorBlock colors = MissionButton.colors;
        colors.normalColor = baseColor;
        colors.highlightedColor = higColor;
        Background.sprite = DisplayedPlanet.PlanetBackground;
        

        MissionButton.colors = colors;
        TradeButton.colors = colors;
        ExitButton.colors = colors;
        TradeUI.enabled = false;
        MissionUI.enabled = false;
        Details.transform.localScale = Vector3.zero;
        mDetailsDisplayed = false;
        
        Mission mission = MissionLog.log.GetActive();
        if (mission == null)
        {
            //Hack-a-round
            mission = GameObject.Find("QuestGiver").GetComponent<QuestGiver>().TradeMission;
            Debug.Log("No mission");
        }
        MissionDescription.text = mission.LongDescription;
        MissionTitle.text = mission.ShortDescription;
    }

    // Update is called once per frame
    void Update () {
	
	}

    public void OnExitSelected() {
        GameObject.DestroyObject(GameObject.Find("PlanetScene"));
        foreach (GameObject gameObject in DisabledGameObjects.disabledGameObjects)
        {
            gameObject.SetActive(true);
        }
        DisabledGameObjects.disabledGameObjects.Clear();
        GameObject.Find("Player").GetComponent<ShipController>().PlayerInSpace = true;
        GameObject.Find("Player").GetComponent<ShipController>().mMoving = false;

    }

    public void OnMissionSelected()
    {
        mSelectedDisplay = mSelectedDisplay == MISSION_DISPLAY ? MAIN_DISPLAY : MISSION_DISPLAY;
        DisplayDetails();
    }

    public void OnTradeSelected()
    {
        mSelectedDisplay = mSelectedDisplay == TRADE_DISPLAY ? MAIN_DISPLAY : TRADE_DISPLAY;
        DisplayDetails();
    }

    void DisplayDetails()
    {
        if (mAnimationInProgress) return;


        if (!mDetailsDisplayed)
        {
            StartCoroutine(AnimateDetailsShow());
            return;
        }

        if (mSelectedDisplay == MAIN_DISPLAY) {
            TradeUI.enabled = false;
            MissionUI.enabled = false;
            Details.transform.localScale = Vector3.zero;
            mDetailsDisplayed = false;
        }

        if (mSelectedDisplay == MISSION_DISPLAY)
        {
            TradeUI.enabled = false;
            MissionUI.enabled = true;

            Mission mission = MissionLog.log.GetActive();
            if (mission == null)
            {
                //Hack-a-round
                mission = GameObject.Find("QuestGiver").GetComponent<QuestGiver>().TradeMission;
                Debug.Log("No mission");
            }
            MissionDescription.text = mission.LongDescription;
            MissionTitle.text = mission.ShortDescription;
        }

        if (mSelectedDisplay == TRADE_DISPLAY)
        {
            TradeUI.enabled = true;
            MissionUI.enabled = false;
        }
    }

    IEnumerator AnimateDetailsShow() {

        mAnimationInProgress = true;
        Vector3 scale = Vector3.zero;
        Details.transform.localScale = scale;

        float t = 0;

        while (t < 1)
        {
            t += Time.deltaTime*2f;
            scale.x = t;
            scale.y = t;
            scale.z = t;

            Details.transform.localScale = scale;

            yield return new WaitForEndOfFrame();
        }

        scale.x = 1;
        scale.y = 1;
        scale.z = 1;
        Details.transform.localScale = scale;

        mDetailsDisplayed = true;
        mAnimationInProgress = false;

        DisplayDetails();
    }
}
