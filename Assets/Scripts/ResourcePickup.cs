﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;


public class ResourcePickup : MonoBehaviour
{
    public ResourceType type;
    public Text PickupText;

    public float volume;

    void Start() {

        PickupText.text = string.Format("{0}", volume);
        var color = PickupText.color;
        switch (type)
        {
            case ResourceType.Fuel:
                color = ToColor(0x0DB479);
                break;
            case ResourceType.Scrap:
                color = ToColor(0xF6B179);
                
                break;
            case ResourceType.Oxygen:
                color = ToColor(0x00BECA);
                break;
            default:
                break;
        }
        color.a = 0f;
        PickupText.color = color;
    }

    void OnTriggerEnter2D(Collider2D other)
    {
        other.GetComponent<ShipResources>().Add(type, volume);
        StartCoroutine(FadeToScore());
        GetComponent<Collider2D>().enabled = false;
    }

    IEnumerator FadeToScore() {
        var color = PickupText.color;

        float t = 0;

        while (t < 1)
        {
            t += Time.deltaTime * 1f;
            color.a = Mathf.Clamp01(1 - t);
            PickupText.color = color;
            GetComponent<SpriteRenderer>().color = color;

            var position = PickupText.gameObject.transform.position;

            position.y += t*0.5f;
            PickupText.gameObject.transform.position = position;

            yield return new WaitForEndOfFrame();
        }

        yield return new WaitForSeconds(1);
        DestroyObject(this.gameObject);
    }

    public Color32 ToColor(int HexVal)
    {
        byte R = (byte)((HexVal >> 16) & 0xFF);
        byte G = (byte)((HexVal >> 8) & 0xFF);
        byte B = (byte)((HexVal) & 0xFF);
        return new Color32(R, G, B, 255);
    }
}