﻿using UnityEngine;
using System.Collections;

public class Spawner : MonoBehaviour
{
    public GameObject[] FuelPrefabs;
    public GameObject[] ScrapPrefabs;
    public GameObject[] OxygenPrefabs;

    public GameObject SpawnAt(ResourceType what, Vector2 where)
    {
        GameObject toInstantiate = null;
        switch (what)
        {
            case ResourceType.Fuel:
                toInstantiate = FuelPrefabs[Random.Range(0, FuelPrefabs.Length)];
                break;
            case ResourceType.Scrap:
                toInstantiate = ScrapPrefabs[Random.Range(0, ScrapPrefabs.Length)];
                break;
            case ResourceType.Oxygen:
                toInstantiate = OxygenPrefabs[Random.Range(0, OxygenPrefabs.Length)];
                break;
        }
        return (GameObject)Instantiate(toInstantiate, where, Quaternion.identity);
    }

}
